class CreateSignUpTokens < ActiveRecord::Migration
  def change
    create_table :sign_up_tokens do |t|
      t.string :email
      t.string :token

      t.timestamps null: false
    end
  end
end
