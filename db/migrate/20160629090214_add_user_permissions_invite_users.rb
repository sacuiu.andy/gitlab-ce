# rubocop:disable all
class AddUserPermissionsInviteUsers < ActiveRecord::Migration
  def up
    add_column :users, :can_invite_users, :boolean, default: false, null: false
  end

  def down
    remove_column :users, :can_invite_users
  end
end
